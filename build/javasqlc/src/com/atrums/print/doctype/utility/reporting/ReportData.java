//Sqlc generated V1.O00-1
package com.atrums.print.doctype.utility.reporting;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class ReportData implements FieldProvider {
static Logger log4j = Logger.getLogger(ReportData.class);
  private String InitRecordNumber="0";
  public String adOrgId;
  public String documentId;
  public String docstatus;
  public String doctypetargetid;
  public String ourreference;
  public String cusreference;
  public String bpartnerId;
  public String bpartnerLanguage;
  public String issalesordertransaction;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("document_id") || fieldName.equals("documentId"))
      return documentId;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("doctypetargetid"))
      return doctypetargetid;
    else if (fieldName.equalsIgnoreCase("ourreference"))
      return ourreference;
    else if (fieldName.equalsIgnoreCase("cusreference"))
      return cusreference;
    else if (fieldName.equalsIgnoreCase("bpartner_id") || fieldName.equals("bpartnerId"))
      return bpartnerId;
    else if (fieldName.equalsIgnoreCase("bpartner_language") || fieldName.equals("bpartnerLanguage"))
      return bpartnerLanguage;
    else if (fieldName.equalsIgnoreCase("issalesordertransaction"))
      return issalesordertransaction;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static ReportData[] dummy(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    return dummy(connectionProvider, cOrderId, 0, 0);
  }

  public static ReportData[] dummy(ConnectionProvider connectionProvider, String cOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "			select" +
      "				'' as ad_Org_Id," +
      "				'' as document_id," +
      "				'' as docstatus," +
      "				'' as docTypeTargetId," +
      "				'' as ourreference," +
      "				'' as cusreference," +
      "				'' as bpartner_id," +
      "				'' as bpartner_language," +
      "				'' as isSalesOrderTransaction" +
      "			from" +
      "				c_order" +
      "			where" +
      "				1=1";
    strSql = strSql + ((cOrderId==null || cOrderId.equals(""))?"":"  			 and c_order.c_order_id in  		" + cOrderId);

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      if (cOrderId != null && !(cOrderId.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ReportData objectReportData = new ReportData();
        objectReportData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectReportData.documentId = UtilSql.getValue(result, "document_id");
        objectReportData.docstatus = UtilSql.getValue(result, "docstatus");
        objectReportData.doctypetargetid = UtilSql.getValue(result, "doctypetargetid");
        objectReportData.ourreference = UtilSql.getValue(result, "ourreference");
        objectReportData.cusreference = UtilSql.getValue(result, "cusreference");
        objectReportData.bpartnerId = UtilSql.getValue(result, "bpartner_id");
        objectReportData.bpartnerLanguage = UtilSql.getValue(result, "bpartner_language");
        objectReportData.issalesordertransaction = UtilSql.getValue(result, "issalesordertransaction");
        objectReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ReportData objectReportData[] = new ReportData[vector.size()];
    vector.copyInto(objectReportData);
    return(objectReportData);
  }

  public static ReportData[] getContratoInfo(ConnectionProvider connectionProvider, String strContratoId)    throws ServletException {
    return getContratoInfo(connectionProvider, strContratoId, 0, 0);
  }

  public static ReportData[] getContratoInfo(ConnectionProvider connectionProvider, String strContratoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "                    select ce.ad_org_id," +
      "						   ce.no_contrato_empleado_id as document_id," +
      "						   ce.isactive as docstatus," +
      "						   ce.c_doctype_id as docTypeTargetId," +
      "						   ce.documentno as ourreference," +
      "						   null as cusreference," +
      "						   ce.c_bpartner_id as bpartner_id," +
      "						   'Y' as isSalesOrderTransaction," +
      "						   bp.ad_language as bpartner_language " +
      "					  from no_contrato_empleado ce" +
      "					  left join c_doctype dt on ce.c_doctype_id = dt.c_doctype_id" +
      "					  left join c_bpartner bp on ce.c_bpartner_id = bp.c_bpartner_id" +
      "					 where ce.no_contrato_empleado_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strContratoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ReportData objectReportData = new ReportData();
        objectReportData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectReportData.documentId = UtilSql.getValue(result, "document_id");
        objectReportData.docstatus = UtilSql.getValue(result, "docstatus");
        objectReportData.doctypetargetid = UtilSql.getValue(result, "doctypetargetid");
        objectReportData.ourreference = UtilSql.getValue(result, "ourreference");
        objectReportData.cusreference = UtilSql.getValue(result, "cusreference");
        objectReportData.bpartnerId = UtilSql.getValue(result, "bpartner_id");
        objectReportData.issalesordertransaction = UtilSql.getValue(result, "issalesordertransaction");
        objectReportData.bpartnerLanguage = UtilSql.getValue(result, "bpartner_language");
        objectReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ReportData objectReportData[] = new ReportData[vector.size()];
    vector.copyInto(objectReportData);
    return(objectReportData);
  }

  public static ReportData[] getRolpagoInfo(ConnectionProvider connectionProvider, String strRolpagoId)    throws ServletException {
    return getRolpagoInfo(connectionProvider, strRolpagoId, 0, 0);
  }

  public static ReportData[] getRolpagoInfo(ConnectionProvider connectionProvider, String strRolpagoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "                    select rol.ad_org_id," +
      "						   rol.no_rol_pago_provision_id as document_id," +
      "						   rol.isactive as docstatus," +
      "						   rol.c_doctype_id as docTypeTargetId," +
      "						   rol.documentno as ourreference," +
      "						   null as cusreference," +
      "						   rol.c_bpartner_id as bpartner_id," +
      "						   'Y' as isSalesOrderTransaction," +
      "						   bp.ad_language as bpartner_language " +
      "					  from no_rol_pago_provision rol" +
      "					  left join c_doctype dt on rol.c_doctype_id = dt.c_doctype_id" +
      "					  left join c_bpartner bp on rol.c_bpartner_id = bp.c_bpartner_id" +
      "					 where rol.no_rol_pago_provision_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strRolpagoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ReportData objectReportData = new ReportData();
        objectReportData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectReportData.documentId = UtilSql.getValue(result, "document_id");
        objectReportData.docstatus = UtilSql.getValue(result, "docstatus");
        objectReportData.doctypetargetid = UtilSql.getValue(result, "doctypetargetid");
        objectReportData.ourreference = UtilSql.getValue(result, "ourreference");
        objectReportData.cusreference = UtilSql.getValue(result, "cusreference");
        objectReportData.bpartnerId = UtilSql.getValue(result, "bpartner_id");
        objectReportData.issalesordertransaction = UtilSql.getValue(result, "issalesordertransaction");
        objectReportData.bpartnerLanguage = UtilSql.getValue(result, "bpartner_language");
        objectReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ReportData objectReportData[] = new ReportData[vector.size()];
    vector.copyInto(objectReportData);
    return(objectReportData);
  }
}
