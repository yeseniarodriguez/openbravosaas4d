//Sqlc generated V1.O00-1
package com.atrums.offline.feletronica.ad_forms;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class CargaArchivosXMLData implements FieldProvider {
static Logger log4j = Logger.getLogger(CargaArchivosXMLData.class);
  private String InitRecordNumber="0";
  public String seqno;
  public String columnname;
  public String startno;
  public String endno;
  public String datatype;
  public String fieldlength;
  public String dataformat;
  public String decimalpoint;
  public String divideby100;
  public String constantvalue;
  public String callout;
  public String name;
  public String referencename;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("seqno"))
      return seqno;
    else if (fieldName.equalsIgnoreCase("columnname"))
      return columnname;
    else if (fieldName.equalsIgnoreCase("startno"))
      return startno;
    else if (fieldName.equalsIgnoreCase("endno"))
      return endno;
    else if (fieldName.equalsIgnoreCase("datatype"))
      return datatype;
    else if (fieldName.equalsIgnoreCase("fieldlength"))
      return fieldlength;
    else if (fieldName.equalsIgnoreCase("dataformat"))
      return dataformat;
    else if (fieldName.equalsIgnoreCase("decimalpoint"))
      return decimalpoint;
    else if (fieldName.equalsIgnoreCase("divideby100"))
      return divideby100;
    else if (fieldName.equalsIgnoreCase("constantvalue"))
      return constantvalue;
    else if (fieldName.equalsIgnoreCase("callout"))
      return callout;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("referencename"))
      return referencename;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CargaArchivosXMLData[] select(ConnectionProvider connectionProvider, String adImpformatId)    throws ServletException {
    return select(connectionProvider, adImpformatId, 0, 0);
  }

  public static CargaArchivosXMLData[] select(ConnectionProvider connectionProvider, String adImpformatId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT f.SeqNo,c.ColumnName,f.StartNo,f.EndNo,f.DataType,c.FieldLength," +
      "        f.DataFormat,f.DecimalPoint,f.DivideBy100,f.ConstantValue,f.Callout, f.Name, r.name AS referenceName" +
      "        FROM AD_ImpFormat_Row f,AD_Column c, AD_Reference r" +
      "        WHERE AD_ImpFormat_ID = ?" +
      "        AND f.AD_Column_ID=c.AD_Column_ID" +
      "        AND r.AD_Reference_ID=c.AD_Reference_ID" +
      "        ORDER BY SeqNo";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adImpformatId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CargaArchivosXMLData objectCargaArchivosXMLData = new CargaArchivosXMLData();
        objectCargaArchivosXMLData.seqno = UtilSql.getValue(result, "seqno");
        objectCargaArchivosXMLData.columnname = UtilSql.getValue(result, "columnname");
        objectCargaArchivosXMLData.startno = UtilSql.getValue(result, "startno");
        objectCargaArchivosXMLData.endno = UtilSql.getValue(result, "endno");
        objectCargaArchivosXMLData.datatype = UtilSql.getValue(result, "datatype");
        objectCargaArchivosXMLData.fieldlength = UtilSql.getValue(result, "fieldlength");
        objectCargaArchivosXMLData.dataformat = UtilSql.getValue(result, "dataformat");
        objectCargaArchivosXMLData.decimalpoint = UtilSql.getValue(result, "decimalpoint");
        objectCargaArchivosXMLData.divideby100 = UtilSql.getValue(result, "divideby100");
        objectCargaArchivosXMLData.constantvalue = UtilSql.getValue(result, "constantvalue");
        objectCargaArchivosXMLData.callout = UtilSql.getValue(result, "callout");
        objectCargaArchivosXMLData.name = UtilSql.getValue(result, "name");
        objectCargaArchivosXMLData.referencename = UtilSql.getValue(result, "referencename");
        objectCargaArchivosXMLData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCargaArchivosXMLData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CargaArchivosXMLData objectCargaArchivosXMLData[] = new CargaArchivosXMLData[vector.size()];
    vector.copyInto(objectCargaArchivosXMLData);
    return(objectCargaArchivosXMLData);
  }

  public static CargaArchivosXMLData[] set()    throws ServletException {
    CargaArchivosXMLData objectCargaArchivosXMLData[] = new CargaArchivosXMLData[1];
    objectCargaArchivosXMLData[0] = new CargaArchivosXMLData();
    objectCargaArchivosXMLData[0].seqno = "";
    objectCargaArchivosXMLData[0].columnname = "";
    objectCargaArchivosXMLData[0].startno = "";
    objectCargaArchivosXMLData[0].endno = "";
    objectCargaArchivosXMLData[0].datatype = "";
    objectCargaArchivosXMLData[0].fieldlength = "";
    objectCargaArchivosXMLData[0].dataformat = "";
    objectCargaArchivosXMLData[0].decimalpoint = "";
    objectCargaArchivosXMLData[0].divideby100 = "";
    objectCargaArchivosXMLData[0].constantvalue = "";
    objectCargaArchivosXMLData[0].callout = "";
    objectCargaArchivosXMLData[0].name = "";
    objectCargaArchivosXMLData[0].referencename = "";
    return objectCargaArchivosXMLData;
  }

  public static String table(ConnectionProvider connectionProvider, String adImpformatId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT t.TableName" +
      "      FROM AD_Table t" +
      "      WHERE t.AD_Table_ID in (SELECT ad_table_id FROM AD_ImpFormat WHERE ad_impformat_id = ?)";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adImpformatId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "tablename");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String parseDate(ConnectionProvider connectionProvider, String text, String format)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select to_char(to_date(?, 'dd/mm/yyyy hh24:mi:ss'), ?) AS FIELD from dual";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, text);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, format);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "field");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String selectSeparator(ConnectionProvider connectionProvider, String impFormat)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT FORMATTYPE FROM AD_IMPFORMAT" +
      "      WHERE AD_IMPFORMAT_ID = ? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, impFormat);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "formattype");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
