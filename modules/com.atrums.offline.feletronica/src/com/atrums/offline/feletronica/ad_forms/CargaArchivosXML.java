/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2009 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.atrums.offline.feletronica.ad_forms;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
/*import org.openbravo.erpCommon.ad_forms.FileImportData;
import org.openbravo.erpCommon.ad_forms.FileLoadData;*/
import com.atrums.offline.feletronica.ad_forms.CargaArchivosXMLData;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class CargaArchivosXML extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
	    VariablesSecureApp vars = new VariablesSecureApp(request);
	    if (log4j.isDebugEnabled())
	      log4j.debug("Command: " + vars.getStringParameter("Command"));

	    if (vars.commandIn("DEFAULT")) {
	      String strAdImpformatId = vars.getStringParameter("inpFile");
	      System.out.println("Directorio1: " + strAdImpformatId);
	      printPage(response, vars, vars.getCommand(), "");
	      System.out.println("INGRESA PRIMER IF: DEFAULT");
	      System.out.println("NOMBRE ARCHIVO: " + vars.getStringParameter("inpFile"));
	    } else if (vars.commandIn("FIND")) {
	    	String strDirectorioFile = vars.getStringParameter("inpFile");
	    	System.out.println("Directorio2: " + strDirectorioFile);
	    	/*
	      String strAdImpformatId = vars.getStringParameter("inpadImpformatId");
	      FieldProvider[] rows = null;
	      String strSeparator = FileImportData.selectSeparator(this, strAdImpformatId);
	      if (log4j.isDebugEnabled())
	        log4j.debug("First Row Header: " + firstRowHeaders);
	      if (strSeparator.equalsIgnoreCase("F"))
	        rows = FileImportData.select(this, strAdImpformatId);
	      fieldsData = new FileLoadData(vars, "inpFile", firstRowHeaders, strSeparator, rows);
	      printSampleImport(vars, fieldsData.getFieldProvider(), request, response, strAdImpformatId,
	          strFirstLineHeader);
	          */
	    	System.out.println("INGRESA SEGUNDO IF: FIND");
	    } else if (vars.commandIn("SAVE")) {
	    	String strAdImpformatId = vars.getStringParameter("inpFile");
	    	/*
	      String strAdImpformatId = vars.getStringParameter("inpadImpformatId");
	      FieldProvider[] rows = null;
	      String strSeparator = FileImportData.selectSeparator(this, strAdImpformatId);
	      if (strSeparator.equalsIgnoreCase("F"))
	        rows = FileImportData.select(this, strAdImpformatId);
	      fieldsData = new FileLoadData(vars, "inpFile", firstRowHeaders, strSeparator, rows);
	      OBError myMessage = importarFichero(vars, fieldsData.getFieldProvider(), request, response,
	          strAdImpformatId);
	      vars.setMessage("FileImport", myMessage);
	      printPageResult(response, vars, "", "SAVE");
	      */
	    	System.out.println("INGRESA TERCER IF: SAVE");
	    } else
	    	System.out.println("INGRESA ERROR");
	        pageError(response);
  }

  
  @SuppressWarnings("unused")
  private String procesarFichero(VariablesSecureApp vars, FieldProvider[] data2,
      HttpServletRequest request, HttpServletResponse response, String strAdImpformatId) throws ServletException, IOException {
    if (data2 == null)
      return "";
    StringBuffer texto = new StringBuffer("");
    CargaArchivosXMLData[] data = CargaArchivosXMLData.select(this, strAdImpformatId);
    if (data == null)
      return "";
    int constant = 0;
    texto.append(
        "<table cellspacing=\"0\" cellpadding=\"0\" width=\"99%\" class=\"DataGrid_Header_Table DataGrid_Body_Table\" style=\"table-layout: auto;\">"
            + "<tr class=\"DataGrid_Body_Row\">  " + "<td>");
    if (log4j.isDebugEnabled())
      log4j.debug("data2.length: " + data2.length);
    for (int i = 0; i < data2.length; i++) {
      if (log4j.isDebugEnabled())
        log4j.debug("i:" + i + " - data.length" + data.length);
      texto.append(
          "<tr class=\"DataGrid_Body_Row DataGrid_Body_Row_" + (i % 2 == 0 ? "0" : "1") + "\">");
      for (int j = 0; j < data.length; j++) {
        if (i == 0 && "N".equalsIgnoreCase("Y"))
          texto.append("<th class=\"DataGrid_Header_Cell\">");
        else
          texto.append("<td class=\"DataGrid_Body_Cell\">");
        if (!data[j].constantvalue.equals("")) {
          constant = constant + 1;
        } else
          texto.append(parseField(data2[i].getField(String.valueOf(j - constant)),
              data[j].fieldlength, data[j].datatype, data[j].dataformat, data[j].decimalpoint, ""));
        if (i == 0 && "N".equalsIgnoreCase("Y"))
          texto.append("</th>");
        else
          texto.append("</td>");
      }
      constant = 0;
      texto.append("</tr>");
    }
    texto.append("</td></table>");
    return texto.toString();
  }
  
  private String parseField(String strTexto, String strLength, String strDataType,
	      String strDataFormat, String strDecimalPoint, String strReferenceName)
	          throws ServletException {
	    if (strReferenceName.equals("TableDir")) {
	      strLength = "33";
	    }
	    if (strDataType.equals("D")) {
	      strTexto = CargaArchivosXMLData.parseDate(this, strTexto, strDataFormat);
	      return strTexto;
	    } else if (strDataType.equals("N")) {
	      if (strDecimalPoint.equals(",")) {
	        strTexto = strTexto.replace('.', ' ').trim();
	        return strTexto.replace(',', '.');
	      } else {
	        return strTexto;
	      }
	    } else {
	      if (log4j.isDebugEnabled())
	        log4j.debug("##########iteration - strTexto:" + strTexto + " - length:" + strLength);
	      int len = Integer.valueOf(strLength).intValue();
	      strTexto = strTexto.substring(0, (len > strTexto.length()) ? strTexto.length() : len);
	      if (log4j.isDebugEnabled())
	        log4j.debug("########## end of iteration - ");
	      return strTexto.replace('\'', '´').trim();
	    }
	  }
  
/*
  private EdiFileImportData insert(ConnectionProvider conn, VariablesSecureApp vars,
      Connection con, EdiFileImportData data) throws ServletException {
    data.iOrderId = SequenceIdData.getUUID();
    data.adClientId = vars.getClient();
    data.adOrgId = vars.getOrg();
    data.adUserId = vars.getUser();
    data.iIsimported = "N";
    data.issotrx = "N";
    data.processing = "N";
    data.processed = "N";
    data.doctypename = "Ped.estándar";
    data.insert(con, conn);
    // clear line data
    data.iOrderId = "";
    data.sku = "";
    data.linedescription = "";
    data.qtyordered = "";

    return data;
  }

  @SuppressWarnings("unused")
private String clean(String oldstr) {
    String newstr = oldstr.substring(0, oldstr.length() - 1);
    return newstr;
  }

  private String get(String str, int pos) {
    String[] tokens = str.split(":");
    if (pos < tokens.length) {
      String result = tokens[pos];
      if (result.endsWith("'"))
        result = result.substring(0, result.length() - 1);
      return result;
    } else {
      log4j.error("getField: requested an out of bounds token");
      return "";
    }
  }

*/
  
  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
	      String strCommand, String texto)
	          throws IOException, ServletException {
	    if (log4j.isDebugEnabled())
	      log4j.debug("Output: Importar Archivo XML");
	    XmlDocument xmlDocument = null;
	    xmlDocument = xmlEngine.readXmlTemplate("com/atrums/offline/feletronica/ad_forms/CargaArchivosXML")
	        .createXmlDocument();

	    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "CargaArchivosXML", false, "", "", "", false,
	        "ad_forms", strReplaceWith, false, true);

	    toolbar.prepareSimpleToolBarTemplate();
	    xmlDocument.setParameter("toolbar", toolbar.toString());
	    if (log4j.isDebugEnabled())
	      log4j.debug("2");

	    try {
	      WindowTabs tabs = new WindowTabs(this, vars, "com/atrums/offline/feletronica/ad_forms/CargaArchivosXML");
	      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
	      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
	      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
	      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "CargaArchivosXML.html",
	          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
	      xmlDocument.setParameter("navigationBar", nav.toString());
	      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CargaArchivosXML.html",
	          strReplaceWith);
	      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
	    } catch (Exception ex) {
	      ex.printStackTrace();
	      throw new ServletException(ex);
	    }

	    xmlDocument.setParameter("theme", vars.getTheme());
	    {
	      OBError myMessage = vars.getMessage("CargaArchivosXML");
	      vars.removeMessage("CargaArchivosXML");
	      if (myMessage != null) {
	        xmlDocument.setParameter("messageType", myMessage.getType());
	        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
	        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
	      }
	    }

	    if (log4j.isDebugEnabled())
	      log4j.debug("3");

	    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
	    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	    if (log4j.isDebugEnabled())
	      log4j.debug("4");


	    response.setContentType("text/html; charset=UTF-8");
	    PrintWriter out = response.getWriter();
	    out.println(xmlDocument.print());
	    out.close();
	  }

  
  public String getServletInfo() {
    return "Servlet that presents the file-importing process";
  } // end of getServletInfo() method
}
