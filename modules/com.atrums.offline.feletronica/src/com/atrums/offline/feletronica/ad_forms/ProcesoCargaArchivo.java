/*
 ******************************************************************************
 * The contents of this file are subject to the   Compiere License  Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * You may obtain a copy of the License at http://www.compiere.org/license.html
 * Software distributed under the License is distributed on an  "AS IS"  basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * The Original Code is                  Compiere  ERP & CRM  Business Solution
 * The Initial Developer of the Original Code is Jorg Janke  and ComPiere, Inc.
 * Portions created by Jorg Janke are Copyright (C) 1999-2001 Jorg Janke, parts
 * created by ComPiere are Copyright (C) ComPiere, Inc.;   All Rights Reserved.
 * Contributor(s): Openbravo SLU
 * Contributions are Copyright (C) 2001-2013 Openbravo S.L.U.
 ******************************************************************************
 */
package com.atrums.offline.feletronica.ad_forms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.modules.ModuleReferenceDataOrgTree;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class ProcesoCargaArchivo extends HttpSecureAppServlet {
	  private static final long serialVersionUID = 1L;
	  private static final String OKTYPE = "Success";

	  @Override
	  public void doPost(HttpServletRequest request, HttpServletResponse response)
	      throws ServletException, IOException {
	    final VariablesSecureApp vars = new VariablesSecureApp(request);
	    if (vars.commandIn("DEFAULT")) {
	    	System.out.println("LLEGA DEFAULT....");
	      printPage(response, vars);
	    } else if (vars.commandIn("OK")) {
	    	org.apache.commons.fileupload.FileItem fileCoAFilePath = vars.getMultiFile("inpFile");
	    	System.out.println("PATH: " + fileCoAFilePath);
	    	
	    	 InputStream istrFileCoA = null;
	    	    try {
	    	      istrFileCoA = fileCoAFilePath.getInputStream();
	    	    } catch (IOException e) {
	    	    	System.out.println("Archivo error: " +fileCoAFilePath.getName());
	    	    }
	    	    
	    	    BufferedReader reader = new BufferedReader(new InputStreamReader(istrFileCoA));
	    	    while(reader.ready()) {
	    	        String line = reader.readLine();
	    	        System.out.println("ARCHIVO: " + line);
	    	   }
	            

	      log4j.debug("ProcesoCargaArchivo - Command OK");

	      /*org.openbravo.erpCommon.businessUtility.InitialOrgSetup ios = new org.openbravo.erpCommon.businessUtility.InitialOrgSetup(
	          OBContext.getOBContext().getCurrentClient());
*/
	      /*OBError obeResult = ios.createOrganization(strOrganization, strOrgUser, strOrgType,
	          strParentOrg, strcLocationId, strPassword, strModules, isTrue(strCreateAccounting),
	          fileCoAFilePath, strCurrency, bBPartner, bProduct, bProject, bCampaign, bSalesRegion,
	          vars.getSessionValue("#SOURCEPATH"));
	      if (!obeResult.getType().equals(OKTYPE)) {
	        OBContext.getOBContext().removeWritableOrganization(ios.getOrgId());
	        OBContext.getOBContext().removeFromWritableOrganization(ios.getOrgId());
	        OBDal.getInstance().rollbackAndClose();
	      }*/
	      /*vars.setSessionValue("#USER_ORG", vars.getSessionValue("#USER_ORG") + ", '" + ios.getOrgId()
	          + "'");
	      vars.setSessionValue("#ORG_CLIENT",
	          vars.getSessionValue("#ORG_CLIENT") + ", '" + ios.getOrgId() + "'");
	      OrgTree tree = new OrgTree(this, vars.getClient());
	      vars.setSessionObject("#CompleteOrgTree", tree);
	      OrgTree accessibleTree = tree.getAccessibleTree(this, vars.getRole());
	      vars.setSessionValue("#AccessibleOrgTree", accessibleTree.toString());
	      //printPageResult(response, vars, ios.getLog(), obeResult);*/
	      
	    } else if (vars.commandIn("CANCEL")) {
	    	System.out.println("LLEGÓ CANCEL");
	    } else
	    	System.out.println("LLEGÓ ERROR");
	      pageError(response);
	  }

private void printPage(HttpServletResponse response, VariablesSecureApp vars) throws IOException,
      ServletException {
	
	System.out.println("LLEGA PRINT PAGE");
    final ModuleReferenceDataOrgTree tree = new ModuleReferenceDataOrgTree(this, vars.getClient(),
        false, true);
    XmlDocument xmlDocument = null;
    final String[] discard = { "selEliminar" };
    if (tree.getData() == null || tree.getData().length == 0)
      xmlDocument = xmlEngine.readXmlTemplate("com/atrums/offline/feletronica/ad_forms/ProcesoCargaArchivo")
          .createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("com/atrums/offline/feletronica/ad_forms/ProcesoCargaArchivo",
          discard).createXmlDocument();
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    final ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "ProcesoCargaArchivo", false, "", "",
        "", false, "ad_forms", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());
    try {
      final WindowTabs tabs = new WindowTabs(this, vars,
          "com.atrums.offline.feletronica.ad_forms.ProcesoCargaArchivo");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      final NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "ProcesoCargaArchivo.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      final LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "ProcesoCargaArchivo.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (final Exception ex) {
      throw new ServletException(ex);
    }
    {
      vars.removeMessage("ProcesoCargaArchivo");
      final OBError myMessage = vars.getMessage("ProcesoCargaArchivo");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }

      response.setContentType("text/html; charset=UTF-8");
      final PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();
    }
  }

  private static boolean isTrue(String s) {
	  
	  System.out.println("LLEGA BOOLEAN");
    if (s == null || s.equals(""))
      return false;
    else
      return true;
  }

  private void printPageResult(HttpServletResponse response, VariablesSecureApp vars,
      String strResult, OBError obeResult) throws IOException, ServletException {
	  
	  System.out.println("LLEGA RESULT....");
    final XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/offline/feletronica/ad_forms/ProcesoCargaArchivo").createXmlDocument();
    String strLanguage = vars.getLanguage();

    xmlDocument.setParameter("resultado",
        Utility.parseTranslation(this, vars, strLanguage, strResult));

    final ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "ProcesoCargaArchivo", false, "", "",
        "", false, "ad_forms", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());
    try {
      final WindowTabs tabs = new WindowTabs(this, vars,
          "com.atrums.offline.feletronica.ad_forms.ProcesoCargaArchivo");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      final NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "ProcesoCargaArchivo.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      final LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "ProcesoCargaArchivo.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (final Exception ex) {
      throw new ServletException(ex);
    }
    if (obeResult != null) {
      xmlDocument.setParameter("messageType", obeResult.getType());
      xmlDocument.setParameter("messageTitle", obeResult.getTitle());
      xmlDocument.setParameter("messageMessage",
          Utility.parseTranslation(this, vars, strLanguage, obeResult.getMessage()));
    }
    response.setContentType("text/html; charset=UTF-8");
    final PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}