package com.atrums.nomina.ad_process;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;

import com.atrums.offline.feletronica.process.ATECOFF_Operacion_Auxiliares;

/**
 * @author ATRUMS-IT
 *
 */
public class NoEnviarEmailRolPagos implements Process {
  static Logger log4j = Logger.getLogger(NoEnviarEmailRolPagos.class);
  final OBError msg = new OBError();

  @Override
  public void execute(ProcessBundle bundle) throws Exception {
    // TODO Auto-generated method stub
    ConnectionProvider conn = bundle.getConnection();
    VariablesSecureApp varsAux = bundle.getContext().toVars();

    OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(),
        varsAux.getOrg());

    NODatosEnvioEmailRolData[] norolesid;

    String tercero = null;
    String email = null;
    String anio = null;
    String mes = null;
    String idRol = null;
    String empresa = null;
    boolean resultado = false;

    try {
    	norolesid = NODatosEnvioEmailRolData.seleccionarRolPagos(conn,
          varsAux.getClient());

      if (norolesid.length > 0) {

    	  tercero = norolesid[0].tercero;
    	  email = norolesid[0].email;
    	  anio = norolesid[0].anio;
    	  mes = norolesid[0].mes;
    	  idRol = norolesid[0].idrol;
    	  empresa = norolesid[0].empresa;

    	  resultado = enviarEmailRol(conn, tercero, email, anio, mes, idRol, empresa);
    	  
    	  if (resultado == true) {
    		  NODatosEnvioEmailRolData.actualizarEnvioRol(conn, idRol);
    	  }else {
    		  NODatosEnvioEmailRolData.actualizarNoEnvioRol(conn, idRol);
    	  }
    	  
      }      
    } catch (Exception ex) {
      Log.error(ex.getMessage(), ex);
      if (resultado == false) {
        NODatosEnvioEmailRolData.actualizarNoEnvioRol(conn, idRol);
      }
    } finally {
      // TODO: handle finally clause
    	norolesid = null;
        OBDal.getInstance().rollbackAndClose();
    }
  } 
  
  public boolean enviarEmailRol(ConnectionProvider connectionProvider, String tercero,
		  String email,String anio,String mes,String idRol,String empresa) throws Exception {

		File flPdf = null;
		String strContenido = "";

        String type = "text/html; charset=utf-8";
        
        strContenido = "<table style=\"width: 85%; padding: 10px; margin:0 auto; border-collapse: collapse;font-family: sans-serif\">\r\n" + 
        		"	<tr style=\"background-color: #003764\">\r\n" + 
        		"		<td style=\"width: 85%\">\r\n" + 
        		"		    <a target=\"_blank\" href=\"http://atrums.com/\">\r\n" + 
        		"			   <img width=\"20%\" style=\"display:block; margin: 1.5% 3%\" src=\"http://ws.atrums.com/portal/images/publicidad/atrumsit-logo.png\">\r\n" + 
        		"			</a></td>\r\n" + 
        		"		<td style=\"width: 3%\">\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.facebook.com/Atrumsit-393603567398708/\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/facebook.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">		\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.instagram.com/atrumsit/?hl=es-la\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/instagram.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">		\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.youtube.com/channel/UCQ6Vl9DHMH3NNa93HxsqAUw?view_as=subscriber\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/youtube.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">	\r\n" + 
        		"			<a target=\"_blank\" href=\"https://twitter.com/atrumsit\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/twitter.png\"/>\r\n" + 
        		"			</a></td>	\r\n" + 
        		"		<td style=\"width: 3%\">\r\n" + 
        		"			<a target=\"_blank\" href=\"https://au.linkedin.com/company/atrumsit\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/linkedin.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"	</tr>\r\n" + 
        		"	<tr>\r\n" + 
        		"		<td style=\"background-color: #ECF0F1\" colspan=\"6\">\r\n" + 
        		"		  <div style=\"border: 6px solid #FFFFFF\">\r\n" + 
        		"			<div style=\"color: #34495e; margin: 2% 6% 2%; text-align: justify;font-size: 14px\">\r\n" + 
        		"				<h2 style=\"color: #8DC63F\">Estimad@:</h2>\r\n" + 
        		"				<p><b>"+ tercero +"</b></p>\r\n" + 
        		"				<p>Usted tiene un nuevo Rol de Pagos generado por la empresa <b>"+ empresa +"</b>, referente al período "+ mes + "  "+ anio +".</p>\r\n" + 
        		"				<div style=\"width: 100%;text-align: center;margin-top: 38px\">\r\n" + 
        		"					Si quiere conocer más de nuestros servicios, haga clic aquí<br><br>\r\n" + 
        		"					<a style=\"text-decoration: none; border-radius: 5px; padding: 8px 18px; color: white; background-color: #8DC63F\" target=\"_blank\" href=\"http://atrums.com/\">ATRUMS IT</a>	\r\n" + 
        		"				</div>	\r\n" + 
        		"				<div style=\"font-size: 12px\">\r\n" + 
        		"					<p style=\"margin-top:50px\">\r\n" + 
        		"					   La información y archivos adjuntos contenidos en este mensaje electrónico son confidenciales y reservados; por tanto no pueden ser usados, reproducidos o divulgados por otras personas distintas a su(s) destinatario(s). \r\n" + 
        		"					   Si Ud. no es el destinatario de este email, le solicitamos comedidamente eliminarlo.\r\n" + 
        		"					</p>\r\n" + 
        		"					<p>\r\n" + 
        		"					   Cualquier novedad comunicarse con Talento Humano.\r\n" + 
        		"					<p>\r\n" +         		
        		"					<p>\r\n" + 
        		"					   Por favor, no responda a este correo electrónico.\r\n" + 
        		"					<p>\r\n" + 
        		"				</div>\r\n" + 
        		"			</div>\r\n" + 
        		"		  </div>\r\n" + 
        		"		</td>\r\n" + 
        		"	</tr>\r\n" + 
        		"	<tr>\r\n" + 
        		"	  <td colspan=\"6\">\r\n" + 
        		"		<table style=\"width: 100%\">\r\n" + 
        		"		    <tr style=\"color: #FFFFFF;background-color: #003764;height: 40px\">\r\n" + 
        		"				<td style = \"width: 100%;font-size: 13px\">\r\n" + 
        		"				    <div style=\"width:98%;margin: 1% 2% 1%\">\r\n" + 
        		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/ubicacion.png\"/>\r\n" + 
        		"							 <a target=\"_blank\" href=\"http://atrums.com/\" style=\"color: #FFFFFF\">www.atrums.com</a>\r\n" + 
        		"						</div>\r\n" + 
        		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/email.png\"/>\r\n" + 
        		"							 <a style=\"text-decoration: none;color: #FFFFFF\">info@atrums.com</a>\r\n" + 
        		"						</div>\r\n" + 
        		"						<div style=\"font-size: 12px\">\r\n" + 
        		"							 <img style=\"width: 10px;\" src=\"http://ws.atrums.com/portal/images/publicidad/telefono.png\"/>\r\n" + 
        		"							 022247848 | 022241461 | <a target=\"_blank\" href=\"https://web.whatsapp.com/\" style=\"color: #FFFFFF\">0958743792</a>\r\n" + 
        		"						</div>\r\n" + 
        		"					</div>\r\n" + 
        		"				</td>\r\n" + 
        		"			</tr>\r\n" + 
        		"		</table>\r\n" + 
        		"	  </td>\r\n" + 
        		"	</tr>\r\n" + 
        		"</table>";
        
        flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(connectionProvider,
				"@basedesign@/com/atrums/nomina/ad_reports/RPT_Rol_Pagos_Individual.jrxml", "Rol de Pagos", idRol);
        List<File> lisdoc = new ArrayList<File>();
        lisdoc.add(flPdf);
        
        if (ATECOFF_Operacion_Auxiliares.enviarCorreo(email, "Entrega Rol de Pagos",
			strContenido, type, lisdoc, false)) {
			flPdf.delete();
		    return true;
		}else {
			return false;
		}
         
	}


}
