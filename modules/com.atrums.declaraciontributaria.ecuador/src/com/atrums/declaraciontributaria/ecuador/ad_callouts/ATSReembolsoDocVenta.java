/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2010 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.atrums.declaraciontributaria.ecuador.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class ATSReembolsoDocVenta extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {

      String cInvoiceId = vars.getStringParameter("inpcInvoiceCompraId");

      try {
        printPage(response, vars, cInvoiceId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String cInvoiceId) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/declaraciontributaria/ecuador/ad_callouts/CallOut").createXmlDocument();

    StringBuffer resultado = new StringBuffer();

    ATSReembolsoVentasData[] data = ATSReembolsoVentasData.selectReembolso(this, cInvoiceId);

    if (data == null || data.length == 0) {

      resultado.append("var respuesta = new Array(new Array(\"inpcDoctypeId\", null),");
      resultado.append("new Array(\"inptipoIdentificacion\", null),");
      resultado.append("new Array(\"inpidentificacion\", null),");
      resultado.append("new Array(\"inpestablecimiento\", null),");
      resultado.append("new Array(\"inpemision\", null),");
      resultado.append("new Array(\"inpsecuencial\", null),");
      resultado.append("new Array(\"inpfechaEmision\", null),");
      resultado.append("new Array(\"inpautorizacionSri\", null),");
      resultado.append("new Array(\"inpbase0\", null),");
      resultado.append("new Array(\"inpbase12\", null),");
      resultado.append("new Array(\"inpbaseExento\", null),");
      resultado.append("new Array(\"inpvalorIce\", null),");
      resultado.append("new Array(\"inpvalorIva\", null),");
      resultado.append("new Array(\"inpcBpartnerId\", null),");
      resultado.append("new Array(\"inpgrandtotal\", null),");
      resultado.append("new Array(\"inpsubtotal\", null));");
    } else {

      if (data != null && data.length > 0) {

        resultado.append("var respuesta = new Array(");
        resultado.append("new Array (\"inpcDoctypeId\", \"" + data[0].getField("dato1")+ "\"),");
        resultado.append("new Array (\"inptipoIdentificacion\", \"" + data[0].getField("dato2")+ "\"),");
        resultado.append("new Array (\"inpidentificacion\", \"" + data[0].getField("dato3")+ "\"),");
        resultado.append("new Array (\"inpestablecimiento\", \"" + data[0].getField("dato4")+ "\"),");
        resultado.append("new Array (\"inpemision\", \"" + data[0].getField("dato5")+ "\"),");
        resultado.append("new Array (\"inpsecuencial\", \"" + data[0].getField("dato6")+ "\"),");
        resultado.append("new Array (\"inpfechaEmision\", \"" + data[0].getField("dato7")+ "\"),");
        resultado.append("new Array (\"inpautorizacionSri\", \"" + data[0].getField("dato8")+ "\"),");
        resultado.append("new Array (\"inpbase0\", \"" + data[0].getField("dato9")+ "\"),");
        resultado.append("new Array (\"inpbase12\", \"" + data[0].getField("dato10")+ "\"),");
        resultado.append("new Array (\"inpbaseExento\", \"" + data[0].getField("dato11")+ "\"),");
        resultado.append("new Array (\"inpvalorIce\", \"" + data[0].getField("dato12")+ "\"),");
        resultado.append("new Array (\"inpvalorIva\", \""+ data[0].getField("dato13") + "\"),");
        resultado.append("new Array (\"inpcBpartnerId\", \""+ data[0].getField("dato14") + "\"),");
        resultado.append("new Array (\"inpgrandtotal\", \""+ data[0].getField("dato15") + "\"),");
        resultado.append("new Array (\"inpsubtotal\", \""+ data[0].getField("dato16") + "\")");
        resultado.append(");");
      }
    }

    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}
